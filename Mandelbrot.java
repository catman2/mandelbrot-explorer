// Basically a java port of http://warp.povusers.org/Mandelbrot/
// Huge thanks the the authors of said page for providing a very well-written article.

import java.awt.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;

public class Mandelbrot
{

	private int[][] imageArray;
	private int[] histogram;
	private int sumIterations;
	private int minIteration;
	private int maxIteration;
	private final double ln2 = Math.log(2);
	private int time = 0;
	private boolean preview;
	private float fColor[];
	private int maxIterations;
	private int windowWidth, windowHeight;
	private double minReal;
	private double maxReal;
	private double minImaginary;
	private double maxImaginary;
	private double omiR, omaR, omiI, omaI;
	private int oI;

	private double realFactor;
	private double imaginaryFactor;

	private double nReal;
	private double nImaginary;

	private double zReal;
	private double zImaginary;
	private double zReal2;
	private double zImaginary2;
	private double zMagnitude;

	private double kReal;
	private double kImaginary;

	public boolean EXIT;
	public boolean drawAborted;

	public Mandelbrot (int width, int height, double minRe, double maxRe, double maxIm, int iter)
	{
		windowWidth = width;
		windowHeight = height;

		omiR = minRe;
		omaR = maxRe;
		omiI = maxIm - (omaR-omiR)*windowHeight/windowWidth;
		omaI = maxIm;
		oI = iter;

		minReal = omiR;
		maxReal = omaR;
		maxImaginary = omaI;
		minImaginary = omiI;

		maxIterations = oI;
	}

	public void zoom(double CONST, int x1, int y1)
	{	//http://stackoverflow.com/questions/5189968/zoom-canvas-to-mouse-cursor
		//this was a MAJOR pain to figure out...

		realFactor = (maxReal-minReal)/(windowWidth-1);
		imaginaryFactor = (maxImaginary-minImaginary)/(windowHeight-1);
		//System.out.println("mouseX: " + x1 + "\tmouseY: " + y1);

		double realCenter = x1 * realFactor + minReal;
		double imaginaryCenter = maxImaginary - y1*imaginaryFactor;
		//System.out.println("real center: " + realCenter + "\timaginary center: " + imaginaryCenter);

		//step 1, translate
		minReal = minReal - realCenter;
		maxReal = maxReal - realCenter;
		minImaginary = minImaginary - imaginaryCenter;
		maxImaginary = maxImaginary - imaginaryCenter;

		//step 2, scale
		minReal = CONST*minReal;
		maxReal = maxReal*CONST;
		minImaginary = CONST*minImaginary;
		maxImaginary = maxImaginary*CONST;

		//step 3, re-translate
		minReal = minReal + realCenter;
		maxReal = maxReal + realCenter;
		minImaginary = minImaginary + imaginaryCenter;
		maxImaginary = maxImaginary + imaginaryCenter;

		if (CONST < 1)
			maxIterations += 1;
		else
			maxIterations -= 1;

	}
	public BufferedImage draw ( boolean fast, int width, int height, Color fC )
	{

		drawAborted = false;

		time++;
		preview = fast;
		if (preview)
		{
			windowWidth = width / 5;
			windowHeight = height / 5;
		}
		else
		{
			windowWidth = width;
			windowHeight = height;
		}

		imageArray = new int[windowWidth+1][windowHeight+1];
		minIteration = maxIterations;
		maxIteration = 0;

		int outsidePoints = 0;
		int insidePoints = 0;


		if (maxIterations < 10)
			maxIterations = 10;
		else if (maxIterations > 10000)
			maxIterations = 10000;

		histogram = new int[99999]; //this is kinda tricky with threading, for now just make sure it's large enough instead of trying to optimize memory usage...
		//System.out.println(maxIterations+2);

		BufferedImage bImage = new BufferedImage(width+1, height+1, BufferedImage.TYPE_INT_RGB);
		fColor = Color.RGBtoHSB( fC.getRed(), fC.getGreen(), fC.getBlue(), null );


		minImaginary = maxImaginary - (maxReal-minReal)*windowHeight/windowWidth;
		//System.out.println("Width: " + width + "\tHeight: " + height);
		realFactor = (maxReal-minReal)/(windowWidth-1);
		imaginaryFactor = (maxImaginary-minImaginary)/(windowHeight-1);

		for (int y=0; y <= windowHeight; y++)
		{
			nImaginary = maxImaginary - y * imaginaryFactor;

			for (int x=0; x <= windowWidth; x++)
			{
				nReal = minReal +  x * realFactor;

				zReal = nReal;
				zImaginary = nImaginary;
				boolean inside = true;
				int currentIteration;
				for (currentIteration = 0; currentIteration < maxIterations; currentIteration++)
				{
					zImaginary2 = zImaginary*zImaginary;
					zReal2 = zReal*zReal;
					//zMagnitude = Math.sqrt(zReal2 + zImaginary2);
					if ( zReal2 + zImaginary2 > 4)
					{
						inside = false;
						break;
					}
					zImaginary = 2 * zReal * zImaginary + nImaginary + kImaginary;
					zReal = zReal2 - zImaginary2 + nReal + kReal;
				}

				if (inside)
				{
					//insidePoints++;
					bImage.setRGB(x, y, 0x000000);
				}
				else
				{
					//outsidePoints++;
					//imageArray[x][y] = currentIteration; //Don't need this for now...
					//histogram[currentIteration]++;
					/*
					sumIterations += currentIteration;
					if ( currentIteration < minIteration )
						minIteration = currentIteration;
					if ( currentIteration > maxIteration )
						maxIteration = currentIteration;
					*/
					bImage.setRGB(x, y, getColor(currentIteration, (double)maxIterations, currentIteration  - (Math.log(Math.log(Math.sqrt(zReal2 + zImaginary2))) / ln2)));
					//bImage.setRGB(x, y, getHistogramColor(currentIteration));
					//System.out.println()
					//System.out.println(currentIteration + "\t\t" + minIteration + "\t\t" + maxIteration);

				}
					//bImage.setRGB(x, y, getColor(currentIteration, (double)maxIterations, currentIteration + 1 - (Math.log(Math.log(Math.sqrt(zReal2 + zImaginary2))) / ln2)));

			}
			if (EXIT == true)
			{
				EXIT = false;
				drawAborted = true;
				//System.out.println("DRAWING ABORTED");
				break;
			}
		}
		/*
			int sum1 = 0;
			for (int i=0; i<maxIterations; i++)
				sum1 += histogram[i];
			int sum2 = 0;
			for (int i=0; i<255; i++)
				sum2 += histogram[i];

		for (int y1 = 0; y1 <= windowHeight; y1++)
		{
			for (int x1 = 0; x1 <= windowWidth; x1++)
				bImage.setRGB(x1, y1, getHistogramColor(imageArray[x1][y1], outsidePoints, sum1, sum2));
		}
		*/
		if (preview == false && drawAborted == false)
			System.out.println("Drawing Mandelbtor:\t" + "minRe: " + minReal + ", maxRe: " + maxReal +
								", minImag: " + minImaginary + ", maxImaginary: " + maxImaginary +
								"\t" + "kReal: " + kReal + ", kImag: " + kImaginary);

		if (preview)
		{
			windowWidth = width;
			windowHeight = height;
			realFactor = (maxReal-minReal)/(windowWidth-1);
			imaginaryFactor = (maxImaginary-minImaginary)/(windowHeight-1);
		}

		return bImage;
	}

	private int getHistogramColor( int currentIteration )
	{
		float percent = (float) currentIteration / (maxIteration);
		int r = (int) percent*255;
		int g = 32;
		int b = 32;
		//if (percent!=0 && percent!=1.0)
		//	System.out.println(percent);
		return r + g*1000 + b*1000000;
	}

	private int oldGetColor(int n, int d, double mu)
	{
		double percent, hue, saturation, brightness;
		percent = ( mu/d );
		hue = fColor[0] + mu/20;
		saturation = 0;
		brightness = 0;
		if (mu<=d)
		{//this is the outside
			saturation = 1.0;
			brightness = percent;
		}
		else if (mu <= 1.5*d)
		{
			saturation = 1-percent;
			brightness = 1;
		}
		return Color.HSBtoRGB((float)hue, (float)saturation, (float)brightness);
	}

	private int getColor( int n, double iter, double mu )
	{	// should be self-explanitory
		float percent = (float) ( mu/(iter/2.0) );
		if ( mu < iter/2 )
			return Color.HSBtoRGB( fColor[0], 1.0f, percent );
		else if (mu >= iter/2 )
			return Color.HSBtoRGB( fColor[0], 0.9f - percent, 1.0f );
		else
			return 0x00FF00;
	}

	public void move(int x1, int y1, double[] fWin)
	{	// leave debug stuff in incase you need them again
		//System.out.println("dX: " + x1 + "\tdY: " + y1);
		double deltaX = x1*fWin[0];
		double deltaY = y1*fWin[1];
		//System.out.println("dReal: " + deltaX + "\tdImaginary: " + deltaY);
		minReal = deltaX + fWin[2];
		maxReal = fWin[3] + deltaX ;
		minImaginary = fWin[4] + deltaY;
		maxImaginary = deltaY + fWin[5];
		//System.out.println("Center: " + zeroY + "\tMin: " + minImaginary + "\tMax: " + maxImaginary);

	}

	public double[] getInfo()
	{	//cats, because it's 3AM and why not.
		double[] cats = new double[6];
		cats[0] = realFactor;
		cats[1] = imaginaryFactor;
		cats[2] = minReal;
		cats[3] = maxReal;
		cats[4] = minImaginary;
		cats[5] = maxImaginary;
		return cats;
	}

	public void setK(double real, double imaginary)
	{	//this one is for setting a specified window
		//minReal = omiR;
		//maxReal = omaR;
		//maxImaginary = omaI;
		//minImaginary = omiI;
		kReal = real;
		kImaginary = imaginary;
	}

	public void setK(int x, int y)
	{ 	//this one is for mouse locations
		minReal = omiR;
		maxReal = omaR;
		maxImaginary = omaI;
		minImaginary = omiI;
		double real = x * realFactor + minReal;
		double imaginary = maxImaginary - y * imaginaryFactor;
		kReal = real;
		kImaginary = imaginary;
	}

	public double[] getK()
	{
		double[] K = {kReal, kImaginary};
		return K;
	}

	public boolean isMandelbrot()
	{
		if (kReal == 0 && kImaginary == 0)
			return true;
		else
			return false;
	}

	public int getIterations() { return maxIterations; }
	public void setIterations(int i) { maxIterations = i; }
	public void reset()
	{
		minReal = omiR;
		maxReal = omaR;
		maxImaginary = omaI;
		minImaginary = omiI;
		maxIterations = oI;
		kReal = 0;
		kImaginary = 0;
	}

	public void kill()
	{
		EXIT = true;
	}

}