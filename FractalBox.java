import java.awt.*;
import javax.swing.*;

public class FractalBox
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame ("Mandelbrot/Julia");
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		FractalPanel cPanel = new FractalPanel();

		frame.getContentPane().add (cPanel);

		frame.pack();
		frame.setVisible(true);
	}
}