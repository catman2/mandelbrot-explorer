# Mandelbrot Explorer

This is a simple Java applet (and application, but we'll get to that in a minute) to plot the Mandelbrot Set in a visually appealing way. You'll want to compile Fractal.java and run it as an applet, if you instead compile and run FractalBox you'll get the older version that among other things lacks multithreaded drawing.

You can visit the project page at https://gibbel.us/activities/mandelbrot_explorer.html for more information.