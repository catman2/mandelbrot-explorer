import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class FractalPanel extends JPanel implements MouseWheelListener, MouseMotionListener, MouseListener, KeyListener
{
	private int panelWidth, panelHeight;
	private Mandelbrot m1;

	int mouseX1, mouseY1, mouseX2, mouseY2, mouseKeyDown;
	double[] fractalWindow;

	Thread drawingThread;

	public FractalPanel()
	{

		panelWidth = 400;
		panelHeight = 400;
		setPreferredSize (new Dimension(panelWidth, panelHeight));
		int iterations = 30;
		m1 = new Mandelbrot( panelWidth, panelHeight, -2.0, 1.0, 1.2, iterations);
        addMouseWheelListener(this);
        addMouseMotionListener(this);
        addMouseListener(this);
        addKeyListener(this);
		this.setFocusable(true);
        this.setDoubleBuffered(false);
	}

	public void paintComponent (Graphics page)
	{
		super.paintComponent (page);
		draw(page);
	}

	private static int randomInRange(int low, int high)
	{
		Random gen = new Random();
		return gen.nextInt(high-low+1) + low;
	}

	public void draw(Graphics page)
	{	// this was supposed to draw a low-res preview first, but it didn't really work. Look into it some more later.
		page.drawImage(m1.draw( true, page.getClipBounds().width, page.getClipBounds().height, new Color( 0,0,192 ) ),
						0, 0, page.getClipBounds().width, page.getClipBounds().height, 0, 0, 40, 40, null);

		long startTime = System.currentTimeMillis();

		page.drawImage(m1.draw( false, page.getClipBounds().width, page.getClipBounds().height, new Color( 0,0,192 ) ),
						0, 0,null);

		long endTime = System.currentTimeMillis();
		System.out.println("Drawing took " + (endTime - startTime) + " milliseconds");
	}

	public void keyPressed(KeyEvent e){
		//System.out.println(e.getKeyCode());
		if (e.getKeyCode() == 27) // esc
		{
			m1.reset();
			repaint();
		}
		else if (e.getKeyCode() == 61 || e.getKeyCode() == 107) // +
		{
			m1.setIterations(m1.getIterations() + 10);
			repaint();
		}
		else if (e.getKeyCode() == 45 || e.getKeyCode() == 109) //
		{
			m1.setIterations(m1.getIterations() - 10);
			repaint();
		}
	}

    public void keyTyped(KeyEvent e)
    {

    }

	public void keyReleased(KeyEvent e)
	{

	}

    public void mousePressed(MouseEvent e)
    {
		if (e.getButton() == MouseEvent.BUTTON1)
		{
			mouseKeyDown = 1;
			mouseX1 = e.getX();
			mouseY1 = e.getY();
			fractalWindow = m1.getInfo();
		}
		else if (e.getButton() == MouseEvent.BUTTON3)
		{
			if (m1.isMandelbrot() == true)
				m1.setK(e.getX(), e.getY() );
			else
				m1.reset();
		}
		repaint();
    }

    public void mouseReleased(MouseEvent e)
    {
		mouseKeyDown = 0;
		repaint();
    }

    public void mouseEntered(MouseEvent e)
    {

    }

    public void mouseExited(MouseEvent e)
    {

    }

    public void mouseClicked(MouseEvent e)
    {

    }

	public void mouseMoved(MouseEvent e)
	{

    }

	public void mouseDragged(MouseEvent e) {
		int dX = (mouseX1 > e.getX())? (mouseX1-e.getX()) : -(e.getX()-mouseX1);
		int dY = (mouseY1 > e.getY())? -(mouseY1-e.getY()) : (e.getY()-mouseY1);;
		m1.move( dX, dY, fractalWindow );
		repaint();

	}


	public void mouseWheelMoved(MouseWheelEvent e)
	{
		repaint();
		int notches = e.getWheelRotation();
		if (notches < 0) {
			m1.zoom(.9, e.getX(), e.getY());
			repaint();
		}
		else
		{
			m1.zoom(1.1, e.getX(), e.getY());
			repaint();
		}
		if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {

		}
		else
		{ //scroll type == MouseWheelEvent.WHEEL_BLOCK_SCROLL

		}
	}

}