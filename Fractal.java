import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;

public class Fractal extends JApplet implements MouseWheelListener, MouseMotionListener, MouseListener, KeyListener, Runnable
{
	private int operatingSystem;	// 0 is *nix, 1 is windows
	private int panelWidth = 800, panelHeight = 600;

	int mouseX1, mouseY1, mouseX2, mouseY2, mouseKeyDown;
	double[] fractalWindow;

	public static Graphics offScreen;
	Graphics2D pageG2;
	public static Image oSI;
	public static Image oSPI;
	public static boolean drawLocked;
	public static boolean redraw;
	public static boolean isDrawing;
	public static boolean needsRepaint;

	public static int[] color = {25, 75, 128};
	public static int[] colorChanges = {0, 0, 0};

	public static  Mandelbrot m1;

	Thread paintThread;

	public void run() {
		boolean cont = false;
		while (cont == false)
		{
			if ((redraw == true || needsRepaint == true) && drawLocked == false)
			{
				//System.out.println("Painting...");
				repaint();
			}
			try
			{
				Thread.currentThread().sleep(20);
			}
			catch(InterruptedException ie) {}
		}
	}

    public void start()
    {
		paintThread = new Thread(this);
		paintThread.start();
    }


	public void init()
	{
		this.setSize(panelWidth, panelHeight);
		m1 = new Mandelbrot( panelWidth, panelHeight, -2.0, 1.0, 1.2, 30);
		addMouseWheelListener(this);
		addMouseMotionListener(this);
		addMouseListener(this);
		this.setFocusable(true);
		addKeyListener(this);
	}

	public void paint(Graphics page)
	{
		//pageG2 = (Graphics2D) page;
		//pageG2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		updateColor();
		if(needsRepaint == true)
		{
			needsRepaint = false;
			drawFinal();
		}

		offScreen = page;
		if (redraw == false)
		{
			drawPreview();
			page.drawImage(oSPI,0, 0, offScreen.getClipBounds().width, offScreen.getClipBounds().height, 0, 0, offScreen.getClipBounds().width/5, offScreen.getClipBounds().height/5, null);

			if(needsRepaint == true)
				needsRepaint = false;
			else
				drawFinal();
		}

		if (redraw == true)
		{
			page.drawImage(oSI, 0, 0,null);
			redraw = false;
		}



	}

	public void drawFinal()
	{
		if ( drawLocked == false && redraw == false)
		{
			new asyncDraw();
		}
		else
		{
			//System.out.println("terminating final draw");
			m1.kill();
		}

	}
	public void drawPreview()
	{

		if ( drawLocked == false  )
		{
			drawLocked = true;
			oSPI = Toolkit.getDefaultToolkit().createImage(m1.draw( true, offScreen.getClipBounds().width, offScreen.getClipBounds().height, new Color( color[0],color[1], color[2] ) ).getSource());
			drawLocked = false;
		}
		else
		{
			//m1.kill();
		}

	}

	public static void updateColor()
	{
		color[0] += 5*colorChanges[0];
		color[1] += 5*colorChanges[1];
		color[2] += 5*colorChanges[2];

		colorChanges[0] = 0;
		colorChanges[1] = 0;
		colorChanges[2] = 0;

		if(color[0] > 255)
			color[0] = 255;
		if(color[0] < 0)
			color[0] = 0;
		if(color[1] > 255)
			color[1] = 255;
		if(color[1] < 0)
			color[1] = 0;
		if(color[2] > 255)
			color[2] = 255;
		if(color[2] < 0)
			color[2] = 0;
	}

	public void keyPressed(KeyEvent e)
	{
		//System.out.println(e.getKeyCode()); // Screw using tables :)
		switch (e.getKeyCode())
		{
			case 27:	m1.reset();	//escape key
						break;

			case 61:
			case 107:	m1.setIterations( (int) (m1.getIterations() * 1.25));	//+
						break;
			case 45:
			case 109:	m1.setIterations( (int) (m1.getIterations() / 1.25));	//-
						break;

			case 38:	m1.setK(m1.getK()[0], m1.getK()[1] - .01 ); //up arrow
						break;
			case 40:	m1.setK(m1.getK()[0], m1.getK()[1] + .01 ); //down arrow
						break;
			case 37:	m1.setK(m1.getK()[0] + .01, m1.getK()[1] ); //left arrow
						break;
			case 39:	m1.setK(m1.getK()[0] - .01, m1.getK()[1] ); //right arrow
						break;

			case 81:	colorChanges[0]++;
						break;
			case 65:	colorChanges[0]--;
						break;
			case 87:	colorChanges[1]++;
						break;
			case 83:	colorChanges[1]--;
						break;
			case 69:	colorChanges[2]++;
						break;
			case 68:	colorChanges[2]--;
						break;

			//case default: break;

		}
		repaint();

	}

	public void keyTyped(KeyEvent e)
	{

	}

	public void keyReleased(KeyEvent e)
	{

	}

	public void mousePressed(MouseEvent e)
	{
		if (e.getButton() == MouseEvent.BUTTON1)
		{
			mouseKeyDown = 1;
			mouseX1 = e.getX();
			mouseY1 = e.getY();
			fractalWindow = m1.getInfo();
		}
		else if (e.getButton() == MouseEvent.BUTTON3)
		{
			if (m1.isMandelbrot() == true)
				m1.setK(e.getX(), e.getY() );
			else
				m1.reset();
			repaint();
		}
	}

	public void mouseReleased(MouseEvent e)
	{
		mouseKeyDown = 0;
		repaint();
	}

	public void mouseEntered(MouseEvent e)
	{

	}

	public void mouseExited(MouseEvent e)
	{

	}

	public void mouseClicked(MouseEvent e)
	{

	}

	public void mouseMoved(MouseEvent e)
	{

	}

	public void mouseDragged(MouseEvent e) {
		//System.out.println(e.getButton());
		//if (e.getButton() == 1)
		//{
			int dX = (mouseX1 > e.getX())? (mouseX1-e.getX()) : -(e.getX()-mouseX1);
			int dY = (mouseY1 > e.getY())? -(mouseY1-e.getY()) : (e.getY()-mouseY1);;
			m1.move( dX, dY, fractalWindow );
			repaint();
		//}

	}


	public void mouseWheelMoved(MouseWheelEvent e)
	{
		//repaint();
		int notches = e.getWheelRotation();
		if (notches < 0) {
			m1.zoom(.9, e.getX(), e.getY());
			repaint();
		}
		else
		{
			m1.zoom(1.1, e.getX(), e.getY());
			repaint();
		}
		if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {

		}
		else
		{ //scroll type == MouseWheelEvent.WHEEL_BLOCK_SCROLL

		}
	}


}


class asyncDraw extends Fractal implements Runnable
{
	//Graphics g1;
	Thread t;
	asyncDraw()
	{
		if (drawLocked == false)
		{
			t = new Thread(this, "async drawing");
			//System.out.println("Child thread: " + t);
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}

	public void run() {


		if ( drawLocked == false )
		{
			drawLocked = true;
			long startTime = System.currentTimeMillis();

			oSI = Toolkit.getDefaultToolkit().createImage(m1.draw( false, offScreen.getClipBounds().width, offScreen.getClipBounds().height, new Color( color[0],color[1], color[2] ) ).getSource());

			long endTime = System.currentTimeMillis();
			drawLocked = false;
			if (m1.drawAborted == false)
			{
				redraw = true;
				//System.out.println("Drew High-res");
				System.out.println("Threaded drawing took " + (endTime - startTime) + " milliseconds");
			}
			else
			{
				//System.out.println("Failed to draw High-res");
				needsRepaint = true;
			}
		}
		//else
		//	System.out.println("Aborted async");


	}

}

